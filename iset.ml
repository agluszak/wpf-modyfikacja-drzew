open List

type interval = {lo: int; hi: int}

type tree = 
    | Empty
    | Node of tree * interval * tree * int * int

type t = tree

let empty = Empty

let is_empty t = t = Empty

let height = function
    | Node(_, _, _, h, _) -> h
    | Empty -> 0

let size = function
    | Node(_, _, _, _, s) -> s
    | Empty -> 0

let interval lo hi = 
    assert (lo <= hi);
    {lo = lo; hi = hi}

(* oblicza rozmiar przedziału, zwraca max_int jeśli jest on zbyt duży *)
let interval_size i = if 1 + i.hi - i.lo <= 0 then max_int else 1 + i.hi - i.lo
    
(* bezpieczny następnik liczby *)
let succ i = if i = max_int then max_int else i + 1

(* bezpieczny poprzednik liczby *)
let pred i = if i = min_int then min_int else i - 1

(* porównanie służące do sortowania przedziałów *)
let disjoint_compare = fun i1 i2 ->
    (* lewy mniejszy *)
    if i1.lo < (pred i2.lo) && i1.hi < (pred i2.lo) then -1
    (* prawy mniejszy *)
    else if i2.lo < (pred i1.lo) && i2.hi < (pred i1.lo) then 1
    (* nie są rozłączne *)
    else 0

(* dodawanie które działa dla bardzo duzych liczb *)
let safe_add a b = if a + b < 0 then max_int else a + b

(* oblicza sumę długości przedziałów w bezpieczny sposób *)
let safe_size l i r = safe_add (safe_add (size l) (size r)) (interval_size i) 

(* składa drzewo avl z wartości w korzeniu i poddrzew, 
    które nie różnią się wysokością o więcej niż 2 *)
let make l i r = Node (l, i, r, max (height l) (height r) + 1, safe_size l i r)

(* balansuje drzewo avl. Może zmienić wysokość o maksymalnie 1 *)
let bal l k r =
  let hl = height l in
  let hr = height r in
  if hl > hr + 2 then
    match l with
    | Node (ll, lk, lr, _, _) ->
        if height ll >= height lr then make ll lk (make lr k r)
        else
          (match lr with 
          | Node (lrl, lrk, lrr, _, _) ->
              make (make ll lk lrl) lrk (make lrr k r)
          | Empty -> assert false)
    | Empty -> assert false
  else if hr > hl + 2 then
    match r with
    | Node (rl, rk, rr, _, _) ->
        if height rr >= height rl then make (make l k rl) rk rr
        else
          (match rl with
          | Node (rll, rlk, rlr, _, _) ->
              make (make l k rll) rlk (make rlr rk rr)
          | Empty -> assert false)
    | Empty -> assert false
  else Node (l, k, r, max hl hr + 1, safe_size l k r)

(* dodaje przedział o którym wiadomo, że jest rozłączny z drzewem *)
let rec add_one i = function
    | Empty -> Node(Empty, i, Empty, 1, interval_size i)
    | Node(l, v, r, h, _) -> 
        let c = disjoint_compare i v in 
        if c = 0 then failwith "Add one not disjoint!"
        else if c < 0 then 
            let nl = add_one i l in
            bal nl v r
        else
            let nr = add_one i r in
            bal l v nr

(* robi avl z dwóch poprawnych bst o rozłącznych 
    zbiorach wartości i wartości w korzeniu *)
let rec join l v r =
  match (l, r) with
    | (Empty, _) -> add_one v r
    | (_, Empty) -> add_one v l
    | (Node(ll, lv, lr, lh, _), Node(rl, rv, rr, rh, _)) ->
        if lh > rh + 2 then bal ll lv (join lr v r) else
        if rh > lh + 2 then bal (join l v rl) rv rr else
        make l v r

(* sprawdza czy punkt należy do przedziału domkniętego *)
let include_compare x i = 
    if x < i.lo then -1 
    else if x > i.hi then 1
    else 0

type 'a option = None | Some of 'a

(* zwraca drzewo avl przedziałów ostro mniejszych od danej liczby
     i opcjonalnie przedział do którego ta liczba należy *)
let rec split_left x = function
    | Empty -> (Empty, None)
    | Node(l, v, r, _, _) ->
        let c = include_compare x v in
        if c = 0 then (l, Some(interval v.lo x))
        else if c < 0 then split_left x l 
        else let (rl, pres) = split_left x r in (join l v rl, pres)

(* zwraca drzewo avl przedziałów ostro większych od danej liczby
     i opcjonalnie przedział do którego ta liczba należy *)
let rec split_right x = function
    | Empty -> (Empty, None)
    | Node(l, v, r, _, _) ->
        let c = include_compare x v in
        if c = 0 then (r, Some(interval x v.hi))
        else if c < 0 then let (lr, pres) = split_right x l in 
            (join lr v r, pres)
        else split_right x r

(* sprawdza czy wartość jest w zbiorze wartości drzewa *)
let rec mem x = function
    | Empty -> false
    | Node(l, v, r, _, _) -> 
        let c = include_compare x v in
        if c = 0 then true 
        else if c < 0 then mem x l
        else mem x r

(* opcjonalnie dołącza do prawidłowego avl jeden przedział, 
    który jest rozłączny i większy *)
let join_left_option l o = match o with
    | None -> l
    | Some(i) -> join l i empty

(* opcjonalnie dołącza do prawidłowego avl jeden przedział, 
    który jest rozłączny i mniejszy *)
let join_right_option r o = match o with
    | None -> r
    | Some(i) -> join empty i r

(* zwraca krotkę: drzewo wartości mniejszych, 
    boola czy dana wartość występuje w drzewie i drzewo wartości większych *)
let rec split x t =
    let smaller = let (lt, li) = split_left (pred x) t in 
        join_left_option lt li
    and greater = let (rt, ri) = split_right (succ x) t in 
        join_right_option rt ri
    and present = mem x t
    in (smaller, present, greater)

(* łączy przedział z opcjonalnym drugim przedziałem
    przedziały muszą się stykać *)
let merge_interval_with_option i o = 
    match o with
    | None -> i
    | Some(j) -> 
        assert ((i.lo - j.hi) < 2 || (j.lo - i.hi) < 2);
        interval (min i.lo j.lo) (max i.hi j.hi)

(* dodaje przedział do drzewa *)
let add_aux i = function 
    | Empty -> make empty i empty
    | Node(_, _, _, _, _) as t -> 
        let (lt, li) = split_left (pred i.lo) t 
        and (rt, ri) = split_right (succ i.hi) t
        in let halfmerged = merge_interval_with_option i li
        in let merged = merge_interval_with_option halfmerged ri
        in join lt merged rt

let add (lo, hi) t = 
    let i = interval lo hi in
    add_aux i t

(* zwraca najmniejszy element drzewa *)
let rec min_elt = function
  | Node (Empty, k, _, _, _) -> k
  | Node (l, _, _, _, _) -> min_elt l
  | Empty -> raise Not_found

(* usuwa najmniejszy element z drzewa *)
let rec remove_min_elt = function
  | Node (Empty, _, r, _, _) -> r
  | Node (l, k, r, _, _) -> bal (remove_min_elt l) k r
  | Empty -> invalid_arg "PSet.remove_min_elt"

(* łączy dwa rozłączne, prawidłowe drzwa, 
    w prawym wszyskie wartości są większe od wartości w pierwszym *)
let merge t1 t2 =
  match t1, t2 with
  | Empty, _ -> t2
  | _, Empty -> t1
  | _ ->
      let k = min_elt t2 in
      bal t1 k (remove_min_elt t2)

(* usuwa przedział z drzwa *)
let remove_aux i = function
    | Empty -> Empty
    | Node(_, _, _, _, _) as t ->
        let (lt, li) = if (i.lo = min_int) then (empty, None) 
            else split_left (pred i.lo) t 
        and (rt, ri) = if (i.hi = max_int) then (empty, None) 
            else split_right (succ i.hi) t
        in let l = join_left_option lt li 
        and r = join_right_option rt ri
        in merge l r

let remove (lo, hi) t = 
    let i = interval lo hi in 
    remove_aux i t

(* lista przedziałów w drzewie w porządku rosnącym *)
let elements t = 
    let rec aux acc = function
        | Empty -> acc
        | Node(l, v, r, _, _) -> aux ((v.lo, v.hi) :: aux acc r) l 
    in aux [] t

let fold f t acc  = 
    let rec aux acc = function
        | Empty -> acc
        | Node(l, v, r, _, _) -> 
            aux (f (v.lo, v.hi) (aux acc l)) r in 
    aux acc t

let iter f t = 
    let rec aux = function
        | Empty -> ()
        | Node(l, v, r, _, _) -> aux l; f (v.lo, v.hi); aux r in 
    aux t

(* zwraca ile w drzewie jest wartości mniejszych bądź równych x *)
let below x t = 
    let (lt, li) = split_left x t in 
        join_left_option lt li |> size